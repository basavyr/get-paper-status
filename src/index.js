var express = require('express');
var app = express();
var request = require('request');
var fs = require('fs');
var cheerio = require('cheerio');
const path = require('path');
const mypath='/home/robert.poenaru/PIPELINE/DevWorkspace/Github/get-paper-status/html';
var content;
/* 
app.use('/', function(req, res, next) {
  var status = 'It works';
  console.log('This is very %s', status);
  //console.log(content);
  next();
}); */

/* request(
  {
    uri:
      'https://authors.aps.org/Submissions/status?utf8=%E2%9C%93&accode=CH10674&author=Poenaru&commit=Submit'
  },
  function(error, response, body) {
    content = body;
  }
); */

request(
  'https://authors.aps.org/Submissions/status?utf8=%E2%9C%93&accode=CH10674&author=Poenaru&commit=Submit',
  (error, res, html) => {
    if (!error && res.statusCode === 200) {
      const $ = cheerio.load(html);
      const details = $('.details');
      const articleInfo = details.find('th').eq(0);
      const articleStatus = details
        .find('th')
        .next()
        .eq(0);
      //console.log(details.html());
      console.log(articleInfo.html());
      console.log(articleStatus.html());
      var stream = fs.createWriteStream(path.join(mypath,'/','status.html'));
      stream.once('open', function() {
        stream.write(
          '<!DOCTYPE html><html lang="en"> <head> <meta charset="UTF-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta http-equiv="X-UA-Compatible" content="ie=edge" /> <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/materia/bootstrap.min.css"rel="stylesheet"integrity="sha384-SYbiks6VdZNAKT8DNoXQZwXAiuUo5/quw6nMKtFlGO/4WwxW86BSTMtgdzzB9JJl"crossorigin="anonymous"/><title>Document</title></head><body> <div class="jumbotron"> <h1 class="display-3">'
        );
        stream.write(articleInfo.html());
        stream.write(articleStatus.html());
        stream.write('</h1> </div></body></html>');
        stream.end();
      });
    }
  }
);

app.get('/nodejs/', function(req, res) {
  console.log('Got a GET request for the homepage');
  res.sendFile(path.join(mypath, '/', 'index.html'));
});
app.get('/nodejs/status', function(req, res) {
  console.log('Got a GET request for the homepage');
  res.sendFile(path.join(mypath, '/', 'status.html'));
});

/* app.get('/url', function(req, res) {
  console.log('You requested a bad thing!!!');
  //console.log(content);
  res.send(content);
  fs.writeFile(
    path.join('/sandbox/html', '/', 'savedHTML.html'),
    content,
    function(err) {
      if (err) {
        return console.log(err);
      }
      console.log('The file was saved!');
    }
  );
});
 */
/* 
app.get('/serve', function(req, res) {
  res.sendFile(path.join('/sandbox/html/', 'parsed.html'));
});
 */
//console.log(__dirname);

/* app.get('/saved', (req, res) => {
  console.log(res);
  res.sendFile(path.join(__dirname, '/', 'savedHTML.html'));
});
 */
var server = app.listen(8001, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Node-App listening at http://%s:%s', host, port);
});
